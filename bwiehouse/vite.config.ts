import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path';

export default defineConfig({
  plugins: [vue()],
  //配置别名
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
  },
  //跨域
  server: {
    cors:true,
    open:true,
    proxy: {
      '/api': {
        target: 'http://127.0.0.1:7001',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ''),
      },
    }
  },
})