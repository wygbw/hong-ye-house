import { home } from './routerChildren/children';
import { createRouter, createWebHistory } from "vue-router";

const routes = [
	{
		path: "/",
		name: "name",
		redirect: "/home/group",
	},
	{
		path: "/home",
		name: "home",
		component: () => import(/* webpackChunkName: "home" */ "@/view/IndexView.vue"),
		children: home
	},
	//登录
	{
		path: "/user/login",
		name: "login",
		component: () => import(/* webpackChunkName: "login" */ "@/view/RegistrationLogin/LoginView.vue"),
	},
	//注册
	{
		path: "/user/registry",
		name: "registry",
		component: () => import(/* webpackChunkName: "registry" */ "@/view/RegistrationLogin/Registration.vue"),
	},
	//直播详情页
	{
		path: "/detail/:id",
		name: "detail",
		component: () => import(/* webpackChunkName: "detail" */ "@/view/DetailView.vue"),
	},
	//审核管理详情
	{
		path: "/examine/detail",
		name: "examinedetail",
		component: () => import(/* webpackChunkName: "examinedetail" */ "@/view/DetailView/Examine/examine.vue"),
	},
	//房源管理的详情
	{
		path: "/resources/detail",
		name: "resourcesdetail",
		component: () => import(/* webpackChunkName: "resourcesdetail" */ "@/view/DetailView/Resources/Resources.vue"),
	},
];

const router = createRouter({
	// 路由模式
	history: createWebHistory(),
	routes,
});
export default router;
