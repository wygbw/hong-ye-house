export const home = [
	///团购管理
	{
		path: "/home/group",
		name: "group",
		component: () => import(/* webpackChunkName: "group" */ "@/view/home/GroupView.vue"),
	},
	///直播管理
	{
		path: "/home/broadcast",
		name: "broadcast",
		component: () => import(/* webpackChunkName: "broadcast" */ "@/view/home/BroadcastView.vue"),
	},

	///房源管理
	{
		path: "/home/resources",
		name: "resources",
		component: () => import(/* webpackChunkName: "resources" */ "@/view/home/ResourcesView.vue"),
	},
	///审核管理
	{
		path: "/home/examine",
		name: "examine",
		component: () => import(/* webpackChunkName: "examine" */ "@/view/home/ExamineView.vue"),
	},
	///订单管理
	{
		path: "/home/order",
		name: "order",
		component: () => import(/* webpackChunkName: "order" */ "@/view/home/OrderView.vue"),
	},
	///资讯管理
	{
		path: "/home/information",
		name: "information",
		component: () => import(/* webpackChunkName: "information" */ "@/view/home/InformationView.vue"),
	},
	///经纪人管理
	{
		path: "/home/broker",
		name: "broker",
		component: () => import(/* webpackChunkName: "broker" */ "@/view/home/BrokerView.vue"),
	},
	///员工管理
	{
		path: "/home/staff",
		name: "staff",
		component: () => import(/* webpackChunkName: "staff" */ "@/view/home/StaffView.vue"),
	},
	///用户管理
	{
		path: "/home/user",
		name: "user",
		component: () => import(/* webpackChunkName: "user" */ "@/view/home/UserView.vue"),
	},
]
