import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "name",
    redirect: "/home/resources",
  },
  {
    path: "/home",
    name: "home",
    component: () => import(/* webpackChunkName: "home" */ "../view/IndexView.vue"),
    children: [
      ///房源管理
      {
        path: "/home/resources",
        name: "resources",
        component: () => import(/* webpackChunkName: "home" */ "../view/home/ResourcesView.vue"),
      },
      //楼盘管理
      {
        path: "/home/properties",
        name: "properties",
        component: () => import(/* webpackChunkName: "home" */ "../view/home/properties/PropertiesView.vue"),
      },
      ///直播管理
      {
        path: "/home/broadcast",
        name: "broadcast",
        component: () => import(/* webpackChunkName: "home" */ "../view/home/BroadcastView.vue"),
      },
      ///团购管理
      {
        path: "/home/buying",
        name: "buying",
        component: () => import(/* webpackChunkName: "home" */ "../view/home/BuyingView.vue"),
      },
      ///审核管理
      {
        path: "/home/examine",
        name: "examine",
        component: () => import(/* webpackChunkName: "examine" */ "../view/home/ExamineView.vue"),
      },
      ///订单管理
      {
        path: "/home/order",
        name: "order",
        component: () => import(/* webpackChunkName: "home" */ "../view/home/OrderView.vue"),
      },
      ///资讯管理
      {
        path: "/home/information",
        name: "information",
        component: () => import(/* webpackChunkName: "home" */ "../view/home/InformationView.vue"),
      },
      ///经纪人管理
      {
        path: "/home/broker",
        name: "broker",
        component: () => import(/* webpackChunkName: "home" */ "../view/home/brokerView.vue"),
      },
      ///员工管理
      {
        path: "/home/staff",
        name: "staff",
        component: () => import(/* webpackChunkName: "home" */ "../view/home/StaffView.vue"),
      },
      ///用户管理
      {
        path: "/home/user",
        name: "user",
        component: () => import(/* webpackChunkName: "home" */ "../view/home/UserView.vue"),
      },
    ],
  },
  //登录
  {
    path: "/user/login",
    name: "login",
    component: () => import(/* webpackChunkName: "home" */ "../view/LoginView.vue"),
  },
  //404
  {
    path: "/:pathMatch(.*)",
    name: "error",
    component: () => import(/* webpackChunkName: "home" */ "../view/ErrorView.vue"),
  },
];

const router = createRouter({
  // 路由模式
  history: createWebHistory(),
  routes,
});
export default router;
