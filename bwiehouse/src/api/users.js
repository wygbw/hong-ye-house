//users.js 用户管理页面
import axios from "../utils/http.js";

//随机验证码
export function getCaptcha() {
  const url = "/user/captcha";
  return axios.get(url);
}

//用户登录(登录)
export function login({ username, password, captcha }) {
  const url = "/user/login";
  return axios.post(url, {
    username,
    password,
    captcha,
  });
}

//新增用户(注册)
export function registry({ username, password }) {
  const url = "/user/registry";
  return axios.post(url, {
    username,
    password,
  });
}

//获取用户列表
export function getUserList() {
  const url = "/getUserList";
  return axios.get(url);
}

//删除用户
export function delUserItem(id) {
  const url = `/user/${id}`;
  return axios.delete(url);
}

//忘记密码
export function forgotPwd({ email, id, subject, text, html }) {
  const url = "/user/forgot";
  return axios.post(url, {
    email,
    id,
    subject,
    text,
    html,
  });
}

//重置密码
export function resetPwd({ id }) {
  const url = `/user/reset/${id}`;
  return axios.get(url);
}

//查询用户信息
export function seeUserItem({ id }) {
  const url = `/user/info/${id}`;
  return axios.get(url);
}

//编辑用户信息
export function editUserItem({ id, nickname, mobile, avatar }) {
  const url = `/user/info/${id}`;
  return request.put(url, {
    nickname,
    mobile,
    avatar,
  });
}

//添加用户信息
export function addUserItem({ id, nickname, username, mobile, email, avatar }) {
  const url = "/user/info";
  return request.post(url, {
    id,
    nickname,
    username,
    mobile,
    email,
    avatar,
  });
}
