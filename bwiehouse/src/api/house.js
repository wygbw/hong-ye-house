//house.js 房源管理
import axios from "../utils/http.ts";

//租房查询接口
export function rentHouse() {
  const url = "/housing";
  return axios.get(url);
}

//删除租房房源接口
export function deleteMarkethouses({ id }) {
  const url = `/deleteMarkethouses/${id}`;
  return axios.delete(url);
}

//修改销售楼盘状态接口
export function setMarketHouseStatus({ id, status }) {
  const url = "/setMarketHouseStatus";
  return axios.post(url, {
    id,
    status,
  });
}

//修改房源管理页面销售经纪人数据接口
export function alterMarketBroker({ id, broker }) {
  const url = "/alterMarketBroker";
  return axios.post(url, {
    id,
    broker,
  });
}

//添加租房房源接口
export function addHouse({
  toward,
  subway,
  lift,
  house_type,
  way,
  img,
  price,
  broker,
  ll,
  building_type,
  section,
  extent,
  name,
  brand,
  periods,
  area,
  feature,
  resident,
  useto,
  pay,
  lease,
  renovation,
  status,
  position,
}) {
  const url = "/addHouse";
  return axios.post(url, {
    toward,
    subway,
    lift,
    house_type,
    way,
    img,
    price,
    broker,
    ll,
    building_type,
    section,
    extent,
    name,
    brand,
    periods,
    area,
    feature,
    resident,
    useto,
    pay,
    lease,
    renovation,
    status,
    position,
  });
}

//修改租房房源接口
export function editHouses({
  toward,
  subway,
  lift,
  house_type,
  way,
  img,
  price,
  broker,
  ll,
  building_type,
  section,
  extent,
  name,
  brand,
  periods,
  area,
  feature,
  resident,
  useto,
  pay,
  lease,
  renovation,
  status,
  position,
  id,
}) {
  const url = "/editHouses";
  return axios.post(url, {
    toward,
    subway,
    lift,
    house_type,
    way,
    img,
    price,
    broker,
    ll,
    building_type,
    section,
    extent,
    name,
    brand,
    periods,
    area,
    feature,
    resident,
    useto,
    pay,
    lease,
    renovation,
    status,
    position,
    id,
  });
}

//获取二手房房源
export function getSecondHouse() {
  const url = "/second/housing";
  return axios.get(url);
}

//删除二手房数据接口
export function deleteRenthouses({ id }) {
  const url = `/deleteRenthouses/${id}`;
  return axios.delete(url);
}

//修改二手房源状态接口
export function setRentHouseStatus({ id, status }) {
  const url = "/setRentHouseStatus";
  return axios.post(url, {
    id,
    status,
  });
}

//修改二手房源经纪人接口
export function alterRentBroker({ id, broker }) {
  const url = "/alterRentBroker";
  return axios.post(url, {
    id,
    broker,
  });
}

//添加二手房房源接口
export function addSecondHouse({
  toward,
  subway,
  lift,
  house_type,
  img,
  price,
  broker,
  ll,
  building_type,
  extent,
  name,
  brand,
  periods,
  area,
  feature,
  resident,
  useto,
  renovation,
  status,
}) {
  const url = "/addSecondHouse";
  return axios.post(url, {
    toward,
    subway,
    lift,
    house_type,
    img,
    price,
    broker,
    ll,
    building_type,
    extent,
    name,
    brand,
    periods,
    area,
    feature,
    resident,
    useto,
    renovation,
    status,
  });
}

//修改二手房房源接口
export function editSecondHouse({
  toward,
  subway,
  lift,
  house_type,
  img,
  price,
  broker,
  ll,
  building_type,
  extent,
  name,
  brand,
  periods,
  area,
  feature,
  resident,
  useto,
  renovation,
  status,
  id,
}) {
  const url = "/editSecondHouse";
  return axios.post(url, {
    toward,
    subway,
    lift,
    house_type,
    img,
    price,
    broker,
    ll,
    building_type,
    extent,
    name,
    brand,
    periods,
    area,
    feature,
    resident,
    useto,
    renovation,
    status,
    id,
  });
}

//获取租房数据
export function renting() {
  const url = "/renting";
  return axios.get(url);
}
