import axios, {
  AxiosInstance,
  AxiosRequestConfig,
  AxiosError,
  AxiosResponse,
} from "axios";

export const BASE_URL = "";

const WHITE_LIST = ["api/login"];

class HTTPClient {
  private instance: AxiosInstance;
  constructor() {
    //创建axios的实例
    this.instance = axios.create({
      baseURL: BASE_URL,
      timeout: 2000,
    });

    //请求拦截
    this.instance.interceptors.request.use(
      (config: any) => {
        //token 添加到请求拦截中(请求头携带token)
        config.headers["Authorization"] =
          "Bearer" + localStorage.getItem("token");
        return config;
      },
      (error) => {
        return Promise.reject(error);
      }
    );

    //响应拦截
    this.instance.interceptors.response.use(
      (response: AxiosResponse) => {
        return response;
      },
      (error) => {
        return Promise.reject(error);
      }
    );
  }

  get<T>(url: string, config?: any): Promise<T> {
    return this.instance.get(url, config);
  }
  post<T>(url: string, data?: any, config?: any): Promise<T> {
    return this.instance.post(url, data, config);
  }
  put<T>(url: string, data?: any, config?: any): Promise<T> {
    return this.instance.put(url, data, config);
  }
  delete<T>(url: string, config?: any): Promise<T> {
    return this.instance.delete(url, config);
  }
}

export default HTTPClient;