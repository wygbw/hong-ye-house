const Service = require('egg').Service;

class ExamineService extends Service {
	//获取审核信息
  async getExamine() {
    const $sql=`select * from controls`;
    return await this.app.mysql.query($sql);
  }
}

module.exports = ExamineService;
