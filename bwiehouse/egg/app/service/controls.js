const Service = require('egg').Service;

class ControlsService extends Service {
	//获取房源信息
  async getControls() {
    const $sql=`select * from controls`;
    return await this.app.mysql.query($sql);
  }
}

module.exports = ControlsService;
