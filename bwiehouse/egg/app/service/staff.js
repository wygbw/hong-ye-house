const Service = require('egg').Service;
const { v4: uuidv4 } = require('uuid');

class UserService extends Service {
	//获取员工信息
  async getStaff() {
    const $sql=`select * from staff`;
    return await this.app.mysql.query($sql);
  }
  //员工删除
  async delStaff({id}){
    const $sql=`delete from staff where id='${id}'`;
    const $params=[id];
    return await this.app.mysql.query($sql,$params);
  }
  //员工添加
  async addStaff({jobNum,name,job,tel}){
    const uid=uuidv4();
    const $sql=`insert into staff (id,jobNum,name,job,tel) values ('${uid}','${jobNum}','${name}','${job}','${tel}')`;
    const $params=[jobNum,name,job,tel];
    return await this.app.mysql.query($sql,$params);
  }
}

module.exports = UserService;
