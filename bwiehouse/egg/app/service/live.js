const Service = require('egg').Service;

class LiveService extends Service {
	//获取直播信息
  async getLive() {
    const $sql=`select * from live`;
    return await this.app.mysql.query($sql);
  }
}

module.exports = LiveService;
