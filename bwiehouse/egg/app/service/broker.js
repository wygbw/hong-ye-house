const Service = require('egg').Service;
const { v4: uuidv4 } = require('uuid');
class UserService extends Service {
	//获取经纪人信息
  async getBroker() {
    const $sql=`select * from brokers`;
    return await this.app.mysql.query($sql);
  }
  //添加经纪人信息
  async addBrokers({name,mobile,company,first_hand_house,second_hand_house,lease_house,status}){
    const uid=uuidv4();
    const $sql=`insert into brokers (id,name,mobile,company,first_hand_house,second_hand_house,lease_house,status) 
    values ('${uid}','${name}','${mobile}','${company}','${first_hand_house}','${second_hand_house}','${lease_house}','${status}')`;
    const $params=[name,mobile,company,first_hand_house,second_hand_house,lease_house,status];
    return await this.app.mysql.query($sql,$params);
  }
  //删除
  async delBrokers({id}){
    const $sql=`delete from brokers where id='${id}'`;
    const $params=[id];
    return await this.app.mysql.query($sql,$params);
  }
}

module.exports = UserService;
