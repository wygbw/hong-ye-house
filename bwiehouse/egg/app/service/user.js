const Service = require("egg").Service;
const { v4: uuidv4 } = require("uuid");
var md5 = require('md5');
class UserService extends Service {
  //添加团购
  async addUserInfo({ img, group_title, house_name, province, city, county, address, start_time, end_time, group_info }) {
    const group_id = uuidv4();
    const $sql = `insert into groups (group_id,img,group_title,house_name,province,city,county,address,start_time,end_time,group_info) 
    values ('${group_id}','${img}','${group_title}','${house_name}','${province}','${city}','${county}','${address}','${start_time}','${end_time}','${group_info}')`;
    const $params = [img, group_title, house_name, province, city, county, address, start_time, end_time, group_info];
    return await this.app.mysql.query($sql, $params);
  }
  //获取团购数据
  async getUserInfo() {
    const $sql = `select * from groups`;
    return await this.app.mysql.query($sql);
  }
  //删除团购
  async delGroups({ group_id }) {
    const $sql = `delete from groups where group_id='${group_id}'`;
    const $params = [group_id];
    return await this.app.mysql.query($sql, $params);
  }
  //用户管理
  async getUser() {
    const $sql = `select * from users`;
    return await this.app.mysql.query($sql);
  }
  //添加用户
  async addUsers({ username, mobile }) {
    const uid = uuidv4();
    const $sql = `insert into users (id,username,mobile) values ('${uid}','${username}','${mobile}')`;
    const $params = [username, mobile];
    return await this.app.mysql.query($sql, $params);
  }
  //删除用户
  async delUsers({ id}) {
    const $sql = `delete from users where id='${id}'`;
    const $params = [id];
    return await this.app.mysql.query($sql, $params);
  }
  //登录
  async login({ username, password }) {
    const $sql = `select * from login where username='${username}' and password ='${password}'`;
    const $params = [username, password];
    return await this.app.mysql.query($sql, $params);
  }
  //注册
  async registry({ username, password }) {
    const uid = uuidv4();
    const $sql = `insert into login (id,username,password) values ('${uid}','${username}','${password}')`;
    const $params = [username, password];
    return await this.app.mysql.query($sql, $params);
  }
}

module.exports = UserService;
