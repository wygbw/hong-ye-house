const Service = require('egg').Service;

class OrderService extends Service {
	//获取订单信息
  async getOrder() {
    const $sql=`select * from orders`;
    return await this.app.mysql.query($sql);
  }
}

module.exports = OrderService;
