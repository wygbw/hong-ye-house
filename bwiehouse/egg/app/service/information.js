const Service = require('egg').Service;

class InformationsService extends Service {
	//获取资讯信息
  async getInformations() {
    const $sql=`select * from infos`;
    return await this.app.mysql.query($sql);
  }
}

module.exports = InformationsService;
