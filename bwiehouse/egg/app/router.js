"use strict";

module.exports = (app) => {
	const {
		router,
		controller
	} = app;
	router.get("/", controller.home.index);
	
	
	//获取用户信息
	router.get("/getUserList", controller.user.getUser);

	
	//添加用户信息
	router.post("/addUsers", controller.user.addUsers)
	//删除用户
	
	router.post("/delUsers", controller.user.delUsers)
	
	//获取房源信息
	router.get("/getControls", controller.controls.getControls);

	//获取直播数据信息
	router.get("/getlive", controller.live.getLive);


	//获取团购信息
	router.get("/housing", controller.user.getUserInfo);

	//获取审核信息
	router.get("/getExamine", controller.examine.getExamine);


	//获取资讯信息
	router.get("/getInformations", controller.informations.getInformations);




	//获取员工信息
	router.get("/getStaff", controller.staff.getStaff);


	//员工删除
	router.post("/delStaff", controller.staff.delStaff);


	//员工添加
	router.post("/addStaff", controller.staff.addStaff)
	
	
	//获取经纪人信息
	router.get("/getbroker", controller.broker.getBroker);
	//添加
	router.post("/addbrokers", controller.broker.addBrokers);
	//删除
	router.post("/delbrokers", controller.broker.delBrokers);


	//获取订单管理信息
	router.get("/getOrder", controller.order.getOrder);

	//验证码
	router.get('/getcaptcha', controller.user.Captcha);



	//登录
	router.post("/user/login", controller.user.login);
	//注册
	router.post("/user/registry", controller.user.registry);

	//添加房源信息
	router.post("/addHouse", controller.user.addUserInfo);



	//删除房源数据
	router.post("/delResources", controller.resources.delResources);


	//团购删除
	router.post("/delGroups", controller.user.delGroups);
};
