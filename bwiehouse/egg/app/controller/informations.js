'use strict';

const { Controller } = require('egg');

class HomeController extends Controller {
  async getInformations() {
    const result = await this.ctx.service.information.getInformations(this.ctx.params);
    if (result.length) {
      this.ctx.body = {
        code: 1,
        messgae: "获取资讯信息成功！",
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        messgae: "获取资讯信息失败！",
      };
    }
  }
}

module.exports = HomeController;
