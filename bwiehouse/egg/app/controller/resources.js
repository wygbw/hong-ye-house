'use strict';

const { Controller } = require('egg');

class HomeController extends Controller {
    //删除房源
  async delResources() {
    const result = await this.ctx.service.resources.delResources(this.ctx.request.body);
    if (result.serverStatus) {
      this.ctx.body = {
        code: 1,
        messgae: "删除房源数据成功！",
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        messgae: "删除房源数据失败！",
      };
    }
  }
}

module.exports = HomeController;
