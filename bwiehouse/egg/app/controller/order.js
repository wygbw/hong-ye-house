'use strict';

const { Controller } = require('egg');

class OrderController extends Controller {
  async getOrder() {
    const result = await this.ctx.service.order.getOrder(this.ctx.params);
    if (result.length) {
      this.ctx.body = {
        code: 1,
        messgae: "获取成功！",
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        messgae: "获取失败！",
      };
    }
  }
}

module.exports = OrderController;
