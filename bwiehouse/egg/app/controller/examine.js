'use strict';

const { Controller } = require('egg');

class HomeController extends Controller {
  async getExamine() {
    const result = await this.ctx.service.examine.getExamine(this.ctx.params);
    if (result.length) {
      this.ctx.body = {
        code: 1,
        messgae: "获取审核信息成功！",
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        messgae: "获取审核信息失败！",
      };
    }
  }
}

module.exports = HomeController;
