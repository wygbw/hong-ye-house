'use strict';

const { Controller } = require('egg');

class HomeController extends Controller {
  async getStaff() {
    const result = await this.ctx.service.staff.getStaff(this.ctx.params);
    if (result.length) {
      this.ctx.body = {
        code: 1,
        messgae: "获取员工数据成功！",
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        messgae: "获取员工数据失败！",
      };
    }
  }
  //删除
  async delStaff() {
    const result = await this.ctx.service.staff.delStaff(this.ctx.request.body);
    if (result.affectedRows) {
      this.ctx.body = {
        code: 1,
        messgae: "删除员工数据成功！",
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        messgae: "删除员工数据失败！",
      };
    }
  }
  //添加
  async addStaff() {
    const result = await this.ctx.service.staff.addStaff(this.ctx.request.body);
    if (result.affectedRows) {
      this.ctx.body = {
        code: 1,
        messgae: "添加员工数据成功！",
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        messgae: "添加员工数据失败！",
      };
    }
  }
}

module.exports = HomeController;
