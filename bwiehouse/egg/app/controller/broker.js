'use strict';

const { Controller } = require('egg');

class HomeController extends Controller {
  //获取
  async getBroker() {
    const result = await this.ctx.service.broker.getBroker(this.ctx.params);
    if (result.length) {
      this.ctx.body = {
        code: 1,
        messgae: "获取成功！",
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        messgae: "获取失败！",
      };
    }
  }
  //添加
  async addBrokers() {
    const result = await this.ctx.service.broker.addBrokers(this.ctx.request.body);
    if (result.affectedRows) {
      this.ctx.body = {
        code: 1,
        message: "添加成功！",
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        message: "添加失败！",
      };
    }
  }
  //删除
  async delBrokers() {
    const result = await this.ctx.service.broker.delBrokers(this.ctx.request.body);
    if (result.affectedRows) {
      this.ctx.body = {
        code: 1,
        message: "删除成功！",
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        message: "删除失败！",
      };
    }
  }
}

module.exports = HomeController;
