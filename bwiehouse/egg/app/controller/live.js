'use strict';

const { Controller } = require('egg');

class HomeController extends Controller {
  async getLive() {
    const result = await this.ctx.service.live.getLive(this.ctx.params);
    if (result.length) {
      this.ctx.body = {
        code: 1,
        messgae: "获取直播数据成功！",
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        messgae: "获取直播数据失败！",
      };
    }
  }
}

module.exports = HomeController;
