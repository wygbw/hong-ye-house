'use strict';

const { Controller } = require('egg');

class HomeController extends Controller {
  async getControls() {
    const result = await this.ctx.service.controls.getControls(this.ctx.params);
    if (result.length) {
      this.ctx.body = {
        code: 1,
        messgae: "获取房源成功！",
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        messgae: "获取房源失败！",
      };
    }
  }
}

module.exports = HomeController;
