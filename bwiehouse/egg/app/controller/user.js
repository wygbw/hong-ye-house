'use strict';

const { Controller } = require('egg');
var jwt = require('jsonwebtoken');
//验证码
var svgCaptcha = require('svg-captcha');
class HomeController extends Controller {
  //添加数据
  async addUserInfo() {
    const result = await this.ctx.service.user.addUserInfo(this.ctx.request.body);
    if (result.affectedRows) {
      this.ctx.body = {
        code: 1,
        messgae: '添加成功！',
      };
    } else {
      this.ctx.body = {
        code: 0,
        messgae: '添加失败！',
      };
    }
  }
  //获取数据
  async getUserInfo() {
    const result = await this.ctx.service.user.getUserInfo(this.ctx.request.body);
    if (result.length) {
      this.ctx.body = {
        code: 1,
        messgae: '获取成功！',
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        messgae: '获取失败！',
      };
    }
  }
  //删除团购数据
  async delGroups() {
    const result = await this.ctx.service.user.delGroups(this.ctx.request.body);
    // console.log(result);
    if (result.affectedRows) {
      this.ctx.body = {
        code: 1,
        message: '删除成功！',
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        message: '删除失败！',
      };
    }
  }
  //获取用户管理信息
  async getUser() {
    const result = await this.ctx.service.user.getUser(this.ctx.request.body);
    if (result.length) {
      this.ctx.body = {
        code: 1,
        messgae: '获取成功！',
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        messgae: '获取失败！',
      };
    }
  }
  //添加
  async addUsers() {
    const result = await this.ctx.service.user.addUsers(this.ctx.request.body);
    if (result.affectedRows) {
      this.ctx.body = {
        code: 1,
        message: '添加用户成功！',
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        message: '添加用户失败！',
      };
    }
  }
  //删除用户
  async delUsers() {
    const result = await this.ctx.service.user.delUsers(this.ctx.request.body);
    if (result.affectedRows) {
      this.ctx.body = {
        code: 1,
        message: '删除成功！',
        result,
      };
    } else {
      this.ctx.body = {
        code: 0,
        message: '删除失败！',
      };
    }
  }
  //登录
  async login() {
    const { username } = this.ctx.request.body;
    var token = jwt.sign(
      {
        username,
      },
      '2012A',
      { expiresIn: 60 * 60 }
    );
    let a = jwt.verify(token, '2012A');
    const result = await this.ctx.service.user.login(this.ctx.request.body);
    if (result.length) {
      this.ctx.body = {
        code: 1,
        messgae: '登录成功！',
        token,
        a,
      };
    } else {
      this.ctx.body = {
        code: 0,
        messgae: '登录失败！',
      };
    }
  }
  //注册
  async registry() {
    const result = await this.ctx.service.user.registry(this.ctx.request.body);
    if (result.affectedRows) {
      this.ctx.body = {
        code: 1,
        message: '注册成功！',
      };
    } else {
      this.ctx.body = {
        code: 0,
        message: '注册失败！',
      };
    }
  }

  //验证码
  async Captcha() {
    var captcha = svgCaptcha.create({
      size: 4, // size of random string
      ignoreChars: '0o1i', // filter out some characters like 0o1i
      noise: 1, // number of noise lines
      color: true, // characters will have distinct colors instead of grey, true if background option is set
      background: '#cc9966', // background color of the svg image
    });
    this.ctx.body = {
      code: 1,
      message: '验证码获取成功！',
      captcha,
    };
  }
}

module.exports = HomeController;
