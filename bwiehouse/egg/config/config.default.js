/* eslint valid-jsdoc: "off" */

'use strict';


module.exports = appInfo => {
  
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1692168852613_3632';

  // add your middleware config here
  config.middleware = [];

  config.mysql = {
  // 单数据库信息配置
  client: {
    // host
    host: 'localhost',
    // 端口号
    port: '3306',
    // 用户名
    user: 'root',
    // 密码
    password: 'sqh0806',
    // 数据库名
    database: 'house',
  },
  // 是否加载到 app 上，默认开启
  app: true,
  // 是否加载到 agent 上，默认关闭
  agent: false,
};
config.security = {
  csrf: false
};
//直播
config.nmsConfig={
  rtmp: {
    port: 1935,
    chunk_size: 60000,
    gop_cache: true,
    ping: 30,
    ping_timeout: 60
  },
  http: {
    port: 8000,
    allow_origin: '*'
  }
}

return config;
};
