const NodeMediaServer = require('node-media-server');


class App {
	constructor(app) {
		this.app = app;
	}

	async didLoad() {
		// console.log(this.app.config.nmsConfig);
		const nms = new NodeMediaServer(this.app.config.nmsConfig)
		nms.run();
	}
}
module.exports = App